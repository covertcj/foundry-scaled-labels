A [Foundry VTT](http://foundryvtt.com/) module to allow scaling labels of tokens, rulers and measurement templates when zoomed out.

If you go to Game Settings / Configure Settings / Module Settings / Scaled Labels, then you can configure what labels to scale.

Install by specifying this manifest URL: https://bitbucket.org/imposeren/foundry-scaled-labels/raw/master/module.json

![example screenshot](https://bitbucket.org/imposeren/foundry-scaled-labels/raw/master/screenshot.png)
